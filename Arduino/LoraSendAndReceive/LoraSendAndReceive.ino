/*
  Lora Send And Receive
  This sketch demonstrates how to send and receive data with the MKR WAN 1300/1310 LoRa module.
  This example code is in the public domain.
*/

#include <MKRWAN.h>
#include <Wire.h>
#include "Adafruit_SHT31.h" // Bibliothèque pour le capteur SHT31

LoRaModem modem;
Adafruit_SHT31 sht31 = Adafruit_SHT31(); // Créer une instance pour le capteur

// Uncomment if using the Murata chip as a module
// LoRaModem modem(Serial1);

#include "arduino_secrets.h"
// Please enter your sensitive data in the Secret tab or arduino_secrets.h
String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;
String devEui = SECRET_DEV_EUI;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial);

  // Initialisation du capteur SHT31
  if (!sht31.begin(0x44)) {  // 0x44 est l'adresse I2C par défaut
    Serial.println("Erreur : Impossible de trouver le capteur SHT31");
    while (1);
  }

  // Configuration du module LoRa
  if (!modem.begin(EU868)) {
    Serial.println("Failed to start module");
    while (1) {}
  };
  Serial.print("Your module version is: ");
  Serial.println(modem.version());
  Serial.print("Your device EUI is: ");
  Serial.println(modem.deviceEUI());

  int connected = modem.joinOTAA(appEui, appKey, devEui);
  if (!connected) {
    Serial.println("Something went wrong; are you indoor? Move near a window and retry");
    while (1) {}
  }

  // Set poll interval to 60 secs.
  modem.minPollInterval(60);
}

void loop() {
  // Lecture de la température à partir du capteur SHT31
  float temp = sht31.readTemperature();

  // Vérification des valeurs lues
  if (isnan(temp)) {
    Serial.println("Erreur de lecture du capteur !");
    return;
  }

  Serial.print("Température: ");
  Serial.print(temp);
  Serial.println(" °C");

  // Envoi de la température via LoRaWAN en tant que message
  String msg = "Temp: " + String(temp) + "C";

  Serial.println();
  Serial.print("Sending: " + msg + " - ");
  for (unsigned int i = 0; i < msg.length(); i++) {
    Serial.print(msg[i] >> 4, HEX);
    Serial.print(msg[i] & 0xF, HEX);
    Serial.print(" ");
  }
  Serial.println();

  int err;
  modem.beginPacket();
  modem.print(msg);
  err = modem.endPacket(true);
  if (err > 0) {
    Serial.println("Message sent correctly!");
  } else {
    Serial.println("Error sending message :(");
  }

  delay(1000);

  // Réception des messages
  if (!modem.available()) {
    Serial.println("No downlink message received at this time.");
    return;
  }
  char rcv[64];
  int i = 0;
  while (modem.available()) {
    rcv[i++] = (char)modem.read();
  }
  Serial.print("Received: ");
  for (unsigned int j = 0; j < i; j++) {
    Serial.print(rcv[j] >> 4, HEX);
    Serial.print(rcv[j] & 0xF, HEX);
    Serial.print(" ");
  }
  Serial.println();
}
